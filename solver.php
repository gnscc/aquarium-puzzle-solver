<?php
$starttime = microtime(true);

// Read statement with octave
exec('octave ./octave/processStatement.m');
exec('ocrad statement.pbm -e numbers_only -o ./txt/constraints.txt -f');

$preMap = str_split(file_get_contents('./txt/statement.txt'));
$n = sqrt(sizeof($preMap));

$constraints = explode(' ',str_replace(PHP_EOL,' ',file_get_contents('./txt/constraints.txt')));
$colConstraints = array_slice($constraints, 0, $n);
$rowConstraints = array_slice($constraints, $n, $n);


$initialMap = [];
for($i = 0; $i < $n; $i++) {
    $aux = [];
    for ($j = 0; $j < $n; $j++) {
        $aux[] = $preMap[$i+$j*$n];
    }
    $initialMap[] = $aux;
}

$n = sizeof($initialMap);
$scripts = [];
$shapesIDs = [];
$shapesMap = [];
$g='_';

$constraintNumber = 1;

// Create variables and search for shapes IDS
for ($i = 0; $i < $n; $i++) {
    for($j = 0; $j < $n; $j++) {
        $scripts[] = "var x".$i.$g.$j." binary;";

        if (!in_array($initialMap[$i][$j], $shapesIDs)) {
            $shapesIDs[] = $initialMap[$i][$j];
        }
        // Creates an array of array of arrays... 
        // Know which cells are from the same shapes and grouped by rows
        // Map->shape->row->col
        $pos = array_keys($shapesIDs, $initialMap[$i][$j])[0];
        $shapesMap[$pos][$i][] = $i.$g.$j;

    }
}

// Constraints for rows and columns
for ($i = 0; $i < $n; $i++) {
    $rowScript = "c".$constraintNumber.": x".$i.$g."0";
    $colScript = "c".($constraintNumber+1).": x"."0".$g.$i;

    for($j = 1; $j < $n; $j++) {
        $rowScript = $rowScript." + x".$i.$g.$j;
        $colScript = $colScript." + x".$j.$g.$i;
    }
    $rowScript = $rowScript." = ".$rowConstraints[$i].";";
    $colScript = $colScript." = ".$colConstraints[$i].";";


    $scripts[] = $rowScript;
    $scripts[] = $colScript;

    $constraintNumber++;
    $constraintNumber++;
}

$auxScripts = [];

//var_dump($shapesMap);
for ($k = 0; $k < sizeof($shapesMap); $k++) {
    $newArray = array_values($shapesMap[$k]);
    for ($i = 0; $i < sizeof($newArray); $i++) {  
        if ($i < sizeof($newArray)-1) {
            $scripts[] = "c".$constraintNumber.": x".$newArray[$i][0]. " <= x".$newArray[$i+1][0].";";
            $constraintNumber++;
        }
        for ($j = 0; $j < sizeof($newArray[$i]); $j++) {
            if ($j < sizeof($newArray[$i])-1) {
                $scripts[] = "c".$constraintNumber.": x".$newArray[$i][$j]." = x".$newArray[$i][$j+1].";";
                $constraintNumber++;
            }
        }
    }
}

// Solve
$scripts [] = 'solve;';
$scripts [] = 'end;';

// Create solver file
$ampl = fopen('solver.ampl','w');

for ($i = 0; $i < sizeof($scripts); $i++) {
    fwrite($ampl, $scripts[$i].PHP_EOL);
}

fclose($ampl);

// Execute script
exec('glpsol -m solver.ampl -w solver.sol');



// Solution to array
$manageFile = explode(" ",str_replace(PHP_EOL, " ", file_get_contents('solver.sol')));

// Print solution
$col = 0;
$solution = [];
$aux_row = [];
for ($i = 0; $i < sizeof($manageFile); $i++) {
    if ($manageFile[$i] == 'j') {
        echo($manageFile[$i+2]);
        $aux_row[] = $manageFile[$i+2];
        $col++;
        if ($col == $n) {
            echo(PHP_EOL);
            $solution[] = $aux_row;
            $aux_row = [];
            $col = 0;
        } else {
            echo(" ");
        }
    }
}


$string = '';
for ($i = 0; $i < $n; $i++) {
    for ($j = 0; $j < $n; $j++) {
        if($solution[$j][$i] == 1) {
            $count++;
            $idx = $i*$n + $j + 1; 
            $string = $string.$idx;
            $string = $string.',';
        }
    }
}

$string = substr($string, 0, -1);

//var_dump($solution);
//var_dump($string);

$starsSol = fopen('./txt/stars.txt','w');
fwrite($starsSol, $string);
fclose($starsSol);

exec('octave ./octave/solutionImg.m');

$endtime = microtime(true);
$timediff = $endtime - $starttime;
echo($timediff.' s'.PHP_EOL);
?>