tic();
close all;
clear all;
pkg load image;

img = imread(strcat(pwd,'/statement.png'));
%binImg = im2bw(img);
imwrite(img, './statement.pbm');
%compImg = imcomplement (binImg);
compImg = imcomplement (img);

% Adjust image only to real statement
cc = bwconncomp(compImg);
bb = regionprops(cc,'Area','FilledImage');
bigIdx = 1 ;
for i=2:size(bb)
  if bb(i).Area >= bb(bigIdx).Area
    bigIdx = i;
  endif
endfor
cleanIm = bb(bigIdx).FilledImage;

% Complement to see little squares as objects
statement = imcomplement(cleanIm(:,:,2));
%imwrite(label2rgb(bwlabel(statement)), './img/cleanStatement.png');

% Centroids of little squares
 squaresBb = regionprops(statement,'Centroid');
 
% Manage statement to erase thin lines
SE = strel('arbitrary',eye(4));
shapesImg = imclose(statement,SE);
labeledImage = bwlabel(shapesImg);
%imwrite(shapesImg, './img/shapesImg.png');

cellNum = size(squaresBb)(1);
n = sqrt(cellNum);
ids = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890←↑→↓·•●–‽‖«»‹›✅❤👍✌✍😀😂😃😄😉😊😍😓😭☠⌘#⌥⌃⇧↩$¢£€¥¬
@
¶
§
©
®
™
°
℃
℉
+
−
×
÷
=
≠
±
√
‰
Ω
∞
≈
~
¹
²
³
ƒ
¼
½
¾
|
⁄
†
‡
․
‥
…
➥
➽
À
Á
Â
Ã
Ä
Å
Æ
Ç
È
É
Ê
Ë
Ì
Í
Î
Ï
Ð
Ñ
Ò
Ó
Ô
Õ
Ö
Ø
Ù
Ú
Û
Ü
Ý
Þ
ß
à
á
â
ã
ä
å
æ
ç
è
é
ê
ë
ì
í
î
ï
ð
ñ
ò
ó
ô
õ
ö
ø
ù
ú
û
ü
ý
þ
ÿ
Ā
ā
Ă
ă
Ą
ą
Ć
ć
Ĉ
ĉ
Ċ
ċ
Č
č
Ď
ď
Đ
đ
Ē
ē
Ĕ
ĕ
Ė
ė
Ę
ę
Ě
ě
Ĝ
ĝ
Ğ
ğ
Ġ
ġ
Ģ
ģ
Ĥ
ĥ
Ħ
ħ
Ĩ
ĩ
Ī
ī
Ĭ
ĭ
Į
į
İ
ı
Ĳ
ĳ
Ĵ
ĵ
Ķ
ķ
ĸ
Ĺ
ĺ
Ļ
ļ
Ľ
ľ
Ŀ
ŀ
Ł
ł
Ń
ń
Ņ
ņ
Ň
ň
ŉ
Ŋ
ŋ
Ō
ō
Ŏ
ŏ
Ő
ő
Œ
œ
Ŕ
ŕ
Ŗ
ŗ
Ř
ř
Ś
ś
Ŝ
ŝ
Ş
ş
Š
š
Ţ
ţ
Ť
ť
Ŧ
ŧ
Ũ
ũ
Ū
ū
Ŭ
ŭ
Ů
ů
Ű
ű
Ų
ų
Ŵ
ŵ
Ŷ
ŷ
Ÿ
Ź
ź
Ż
ż
Ž
☚
☛
☜
☝
☞
☟
★
☆
✔
♠
♣
♥
♦
♪
♫
♯
♀
♂';

% Sort centroids in squaresBb so up-down, left-right
for j = 1:n:cellNum
    for k=1:n
      A(end+1) = squaresBb(j+k-1).Centroid(1);
      B(end+1) = squaresBb(j+k-1).Centroid(2);
    end;
    A(j:j+n-1) = mean(A(j:j+n-1));
end;
B = sort(B);

for j = 1:n:cellNum
  for k=1:n
    squaresBb(j+k-1).Centroid(1) = A(j+k-1);
    squaresBb(j+k-1).Centroid(2) = B((j+n-1)/n+(k-1)*n);
  end;
end;

% Build an output: firstCol.secondCol...
referencePoints = [];
referenceIds = "";
found = -1;
for i = 1:cellNum
  % Take centroid
  x = round(squaresBb(i).Centroid(1));
  y = round(squaresBb(i).Centroid(2));
  
  siz = size(referencePoints)(2);
  for j=1:2:siz
    % Check if centroid is in the same shape as someone in reference
    if labeledImage(y, x) == labeledImage(referencePoints(j+1), referencePoints(j))
      % Take same value as reference
      output(i) = referenceIds((j+1)/2);
      found = 1;
      break;
    end;
  end;
  if (found==1)
    found = 0;
    continue;
  end;
  
  % If centroid is not in the same shape as someone in reference
  % Then it becomes a reference
  output(i) = ids(1);
  ids(1) = "";
  referenceIds(end+1) = output(i) ;
  referencePoints(end+1) = x;
  referencePoints(end+1) = y;
end;

statementText = fopen('./txt/statement.txt','w');
fprintf(statementText,output);
statementText = fclose(statementText);

% save reusuable variables to build an image with the solution
save ./mat/squaresBb.mat squaresBb;
save ./mat/statement.mat statement;

elapsed_time=toc()

